package zorm

import (
	"context"
	"errors"
	"log"
	"time"
)

type Engine struct {
	dbDao    *DBDao
	ctx      context.Context
}

func NewEngine(dataSourceConfig *DataSourceConfig) (*Engine,error) {
	db, err :=newDBDao(dataSourceConfig)
	newCtx, err := db.BindContextDBConnection(context.Background())
	if err != nil {
		return nil, err
	}
	return &Engine{
		dbDao: db,
		ctx: newCtx,
	},nil
}


func (engine *Engine) SelectTable(tableNameOrBean string, selectCols ...string) *Session {

	return &Session{
		finderTable: NewSelectFinder(wrapTableName(tableNameOrBean), selectCols...),
		page:        nil,
		engine:      engine,
	}
}

func (engine *Engine) SelectMap(tableNameOrBean string, entity IEntityStruct) *Session {

	return &Session{
		finderTable: NewSelectFinder(wrapTableName(tableNameOrBean), wrapMapSelectCols(entity)),
		page:        nil,
		engine:      engine,
	}
}

func (engine *Engine) DeleteTable(tableNameOrBean string) *Session {

	return &Session{
		finderTable: NewDeleteFinder(wrapTableName(tableNameOrBean)),
		page:        nil,
		engine:      engine,
	}
}

func (engine *Engine) UpdateTable(tableNameOrBean string) *Session {

	return &Session{
		finderTable: NewUpdateFinder(wrapTableName(tableNameOrBean)),
		page:        nil,
		engine:      engine,
	}
}

func (engine *Engine) SelectSql(sql string, args ...interface{}) *Session {
	finder := NewFinder()
	finder.Append(sql, args)
	return &Session{
		finderTable: finder,
		page:        nil,
		engine:      engine,
	}
}


func (engine *Engine) Insert(entity IEntityStruct) error {
	var err error
	
	_, err = Transaction(engine.ctx, func(ctx context.Context) (interface{}, error) {
		autoSetTime(entity, TagMap{"created", time.Now()})
		_, err := Insert(engine.ctx, entity)
		return nil, err
	})
	return err
}

func (engine *Engine) InsertMap(entity IEntityStruct) error {
	var err error
	
	_, err = Transaction(engine.ctx, func(ctx context.Context) (interface{}, error) {
		autoSetTime(entity, TagMap{"created", time.Now()}, TagMap{"updated", time.Now()})
		insertFinder, err := genInsertSql(entity)
		if err != nil {
			return nil, err
		}
		_, err = UpdateFinder(engine.ctx, insertFinder)
		return nil, err
	})
	return err
}

func (engine *Engine) InsertSlice(entityStruct interface{}) error {
	entityStructSlice, ok := entityStruct.([]IEntityStruct)
	if !ok {
		return errors.New("插入对象非[]IEntityStruct类型")
	}
	var err error
	if entityStructSlice == nil || len(entityStructSlice) == 0 {
		
		return nil
	}
	
	_, err = Transaction(engine.ctx, func(ctx context.Context) (interface{}, error) {
		for _, entityStruct := range entityStructSlice {
			autoSetTime(entityStruct, TagMap{"created", time.Now()}, TagMap{"updated", time.Now()})
		}

		_, err := InsertSlice(engine.ctx, entityStructSlice)
		return nil, err
	})
	return err
}



func (engine *Engine) Exec(sql string, args ...interface{}) error {
	var err error
	
	_, err = Transaction(engine.ctx, func(ctx context.Context) (interface{}, error) {
		finder := NewFinder()
		finder.SelectTotalCount = false
		finder.Append(sql, args)
		_, err := UpdateFinder(engine.ctx, finder)
		return nil, err
	})
	return err
}

func (engine *Engine) NewEngine() (*Engine, error) {
	newCtx, err := engine.dbDao.BindContextDBConnection(context.Background())
	if err != nil {
		log.Fatalf("change ctx error: %s", err)
		return nil, err
	}
	return &Engine{
		dbDao: engine.dbDao,
		ctx:   newCtx,
	}, nil
}
