基于ZORM框架参考XORM语法,方便XORM框架进行快速迁移,测试用例如下:

```go
package main

import (
	"fmt"
	_ "gitee.com/chunanyong/dm"
	"gitee.com/chunanyong/zorm"
	"log"
	"time"
)

var DB=zorm.Engine{}
func InitDb()  {
	dbConfig := &zorm.DataSourceConfig{
		DSN:                   "dm://SYSDBA:123456789@127.0.0.1:5236/SYSDBA",
		DriverName:            "dm",
		Dialect:               "dm",
		MaxIdleConns:          16,
		MaxOpenConns:          128,
		ConnMaxLifetimeSecond: 600,
		SlowSQLMillis:         1,
	}
	db, err := zorm.NewEngine(dbConfig)
	if err != nil {
		log.Fatalf("cannot connect DmDB[%s]: %v", dbConfig.DSN, err)
	}
	DB=*db
}
const TestTableName = "test"

type Test struct {
	zorm.EntityStruct
	Id            int64     `column:"id"`
	Title         string    `column:"title"`
	Content       string    `column:"content"`
	UpdateTime    time.Time `column:"update_time" zorm:"updated"`
	CreateTime 	  time.Time `column:"create_time" zorm:"created"`
}

func (test *Test) GetTableName() string {
	return TestTableName
}
func (test *Test) GetPKColumnName() string {
	return "id"
}
func main()  {
	InitDb()
	//testInsert()
	testUpdateCols()
	//testUpdateEntity()
	//testFindAll()
	//testFindOne()
	//testDelete()
}
func testInsert()  {
	test:=&Test{
		Title: "Title",
		Content: "Content",
	}
	err:=DB.Insert(test)
	fmt.Println(err)
}
func testUpdateCols()  {
	test:=&Test{
		Title: "TitleUpdate",
	}
	err:=DB.UpdateTable(TestTableName).Where("id=?",1).SetColsByEntity(test,"title").Commit()
	fmt.Println(err)
}
func testUpdateEntity()  {
	test:=&Test{
		Title: "TitleUpdate",
		Content: "ContentUpdate",
	}
	err:=DB.UpdateTable(TestTableName).Where("id=?",4).SetEntity(test).Commit()
	fmt.Println(err)
}
func testFindAll()  {
	var testList []Test
	err:=DB.SelectTable(TestTableName).FindAll(&testList)
	fmt.Println(err)
	fmt.Println(len(testList))
}
func testFindOne()  {
	var test Test
	has,err:=DB.SelectTable(TestTableName).Where("id=?",1).FindOne(&test)
	fmt.Println(has)
	fmt.Println(err)
	fmt.Println(test.Title)
}
func testDelete()  {
	err:=DB.DeleteTable(TestTableName).Where("title=?","TitleDelete").Commit()
	fmt.Println(err)
}
```