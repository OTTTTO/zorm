package zorm

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"reflect"
	"strings"
	"time"
)

type TagMap struct {
	key   string
	value interface{}
}

func (session *Session) checkJoin() {
	if session.finderJoin == nil {
		session.finderJoin = NewFinder()
	}
}

func (session *Session) checkSet() {
	if session.finderSet == nil {
		session.finderSet = NewFinder()
	}
}

func (session *Session) checkOrderBy() {
	if session.finderOrderBy == nil {
		session.finderOrderBy = NewFinder()
	}
	sqlStr, err := session.finderOrderBy.GetSQL()
	if err != nil {
		log.Println(err.Error())

	}
	if strings.Contains(sqlStr, "order by") {
		session.finderOrderBy.Append(" , ")
	} else {
		session.finderOrderBy.Append(" order by ")
	}
}

func (session *Session) checkWhere(linkKey string) {
	if session.finderWhere == nil {
		session.finderWhere = NewFinder()
	}
	sqlStr, err := session.finderWhere.GetSQL()
	if err != nil {
		log.Println(err.Error())

	}
	if strings.Contains(sqlStr, "where") {
		session.finderWhere.Append(" " + linkKey + " ")
	} else {
		session.finderWhere.Append(" where ")
	}
}

func (session *Session) appendWhere(where string, args ...interface{}) {
	session.finderWhere.Append("(")
	session.finderWhere.Append(where, args...)
	session.finderWhere.Append(")")
}

func (session *Session) genFinder() *Finder {

	defer func() {
		if err := recover(); err != nil {
			log.Fatalf("拼接Finder时产生错误: %s", err)
			return
		}
	}()

	finder := session.finderTable
	var err error

	if session.finderSet != nil {
		finder, err = finder.AppendFinder(session.finderSet)
		if err != nil {
			log.Fatalf("DmDb:append [set] finder error: %s", err.Error())
			return nil
		}
	}

	if session.finderJoin != nil {
		finder, err = finder.AppendFinder(session.finderJoin)
		if err != nil {
			log.Fatalf("DmDb:append [join] finder error: %s", err.Error())
			return nil
		}
	}

	if session.finderWhere != nil {
		finder, err = finder.AppendFinder(session.finderWhere)
		if err != nil {
			log.Fatalf("DmDb:append [where] finder error: %s", err.Error())
			return nil
		}
	}

	if session.finderOrderBy != nil {
		finder, err = finder.AppendFinder(session.finderOrderBy)
		if err != nil {
			log.Fatalf("DmDb:append [orderBy] finder error: %s", err.Error())
			return nil
		}
	}
	return finder
}

func calcLen(args []interface{}) int {
	length := 0
	if args == nil {
		return length
	}
	for _, arg := range args {
		ins := reflect.ValueOf(arg)
		if ins.Type().Kind() == reflect.Slice {
			length += ins.Len()
		} else if arg != nil {
			length += 1
		}
	}
	return length
}

func autoSetTime(entity interface{}, tagMapList ...TagMap) string {
	const zormTagName = "zorm"
	const columnTagName = "column"
	var columnName string
	entityValue := reflect.ValueOf(entity)
	kind := entityValue.Kind()
	if kind == reflect.Ptr {
		entityType := entityValue.Elem().Type()
		for i := 0; i < entityType.NumField(); i++ {
			fieldType := entityType.Field(i)
			tag := fieldType.Tag.Get(zormTagName)
			for _, tagMap := range tagMapList {
				if tag == tagMap.key {
					value := entityValue.Elem().Field(i)
					value.Set(reflect.ValueOf(tagMap.value))
					columnName = fieldType.Tag.Get(columnTagName)
				}
			}
		}
	}
	return columnName
}

func autoSetCols(entity IEntityStruct, cols ...string) (string, []interface{}) {
	const tagName = "column"
	var args []interface{}
	colStr := strings.Builder{}
	entityValue := reflect.ValueOf(entity)
	kind := entityValue.Kind()
	if kind == reflect.Ptr {
		entityType := entityValue.Elem().Type()

		for _, col := range cols {
			//field:=entityValue.Elem().FieldByName(col)
			for i := 0; i < entityType.NumField(); i++ {
				fieldType := entityType.Field(i)
				tagValue := fieldType.Tag.Get(tagName)
				if tagValue == col && tagValue != "" {
					field := entityValue.Elem().Field(i)
					if field.IsValid() {
						isDefaultTime,colValue, _ := handleSliceOrMapCol(field)
						if isDefaultTime{
							continue
						}
						args = append(args, colValue)
						if colStr.Len() > 0 {
							colStr.WriteString(",")
							colStr.WriteString(col)
						} else {
							colStr.WriteString(col)
						}
						colStr.WriteString("= ? ")
					}
				}
			}
		}
	}
	return colStr.String(), args
}
func autoSetEntity(entity IEntityStruct) (string, []interface{}) {
	const tagName = "column"
	var args []interface{}
	colStr := strings.Builder{}
	entityValue := reflect.ValueOf(entity)
	kind := entityValue.Kind()
	if kind == reflect.Ptr {
		entityType := entityValue.Elem().Type()
		for i := 0; i < entityType.NumField(); i++ {
			fieldType := entityType.Field(i)
			tagValue := fieldType.Tag.Get(tagName)
			if tagValue != entity.GetPKColumnName() && tagValue != "" {
				field := entityValue.Elem().Field(i)
				if field.IsValid() {
					isDefaultTime,colValue, _ := handleSliceOrMapCol(field)
					if isDefaultTime{
						continue
					}
					args = append(args, colValue)
					if colStr.Len() > 0 {
						colStr.WriteString(",")
						colStr.WriteString(tagValue)
					} else {
						colStr.WriteString(tagValue)
					}
					colStr.WriteString("= ? ")
				}
			}
		}
	}
	return colStr.String(), args
}
func autoSetColsByMap(entityMap map[string]interface{}) (string, []interface{}) {
	var args []interface{}
	colStr := strings.Builder{}
	for key := range entityMap {
		args = append(args, entityMap[key])
		if colStr.Len() > 0 {
			colStr.WriteString(",")
			colStr.WriteString(key)
		} else {
			colStr.WriteString(key)
		}
		colStr.WriteString("= ? ")
	}

	return colStr.String(), args
}

func SetNilList(obj interface{}) {
	if obj != nil {
		value := reflect.ValueOf(obj)
		if value.Elem().Len() == 0 {
			valueType := value.Type()
			value.Elem().Set(reflect.MakeSlice(valueType.Elem(), 0, 1))
		}
	}
}

func wrapTableName(tableName string) string {
	return fmt.Sprintf("\"%s\"", tableName)
}

func setSliceValueByMap(entitySlice interface{}, valueMapList []map[string]interface{}) error {
	const zormTagName = "zorm"
	const tableTagName = "table"
	//var args []interface{}
	//colStr := strings.Builder{}
	if entitySlice == nil {
		return errors.New("query数组必须是*[]struct类型或者*[]*struct或者基础类型数组的指针")
	}
	pv1 := reflect.ValueOf(entitySlice)
	if pv1.Kind() != reflect.Ptr { //如果不是指针
		return errors.New("query数组必须是*[]struct类型或者*[]*struct或者基础类型数组的指针")
	}
	//获取数组元素
	//Get array elements
	sliceValue := reflect.Indirect(pv1)

	//如果不是数组
	//If it is not an array.
	if sliceValue.Kind() != reflect.Slice {
		return errors.New("query数组必须是*[]struct类型或者*[]*struct或者基础类型数组的指针")
	}
	//获取数组内的元素类型
	//Get the element type in the array
	sliceElementType := sliceValue.Type().Elem()
	//slice数组里是否是指针,实际参数类似 *[]*struct,兼容这种类型
	sliceElementTypePtr := false
	//如果数组里还是指针类型
	if sliceElementType.Kind() == reflect.Ptr {
		sliceElementTypePtr = true
		sliceElementType = sliceElementType.Elem()
	}
	entityType := sliceElementType
	for _, valueMap := range valueMapList {
		pv := reflect.New(sliceElementType)
		for i := 0; i < entityType.NumField(); i++ {
			pvValue := pv.Elem().Field(i)
			fieldStruct := entityType.Field(i)
			zormTagValue := fieldStruct.Tag.Get(zormTagName)

			if zormTagValue == "extends" {
				if fieldStruct.Type.Kind() == reflect.Struct {
					childType := fieldStruct.Type
					childPv := reflect.New(childType)
					tableName := fieldStruct.Tag.Get(tableTagName)
					for j := 0; j < childType.NumField(); j++ {
						childPvValue := childPv.Elem().Field(j)
						childFieldStruct := childType.Field(j)
						err := setColumnValueByMap(&childPvValue, childFieldStruct, valueMap, tableName)
						if err != nil {
							return err
						}
					}
					pvValue.Set(childPv.Elem())
				}
			} else {
				err := setColumnValueByMap(&pvValue, fieldStruct, valueMap, "")
				if err != nil {
					return err
				}
			}
		}
		if sliceElementTypePtr { //如果数组里是指针地址,*[]*struct
			sliceValue.Set(reflect.Append(sliceValue, pv))
		} else {
			sliceValue.Set(reflect.Append(sliceValue, pv.Elem()))
		}
	}

	return nil
}
func setObjValueByMap(entity interface{}, valueMap map[string]interface{}) error {
	const zormTagName = "zorm"
	const tableTagName = "table"
	entityValue := reflect.ValueOf(entity)
	entityType := entityValue.Elem().Type()
	pv := reflect.New(entityType)
	for i := 0; i < entityType.NumField(); i++ {
		pvValue := pv.Elem().Field(i)
		fieldStruct := entityType.Field(i)
		zormTagValue := fieldStruct.Tag.Get(zormTagName)

		if zormTagValue == "extends" {
			if fieldStruct.Type.Kind() == reflect.Struct {
				childType := fieldStruct.Type
				childPv := reflect.New(childType)
				tableName := fieldStruct.Tag.Get(tableTagName)
				for j := 0; j < childType.NumField(); j++ {
					childPvValue := childPv.Elem().Field(j)
					childFieldStruct := childType.Field(j)
					err := setColumnValueByMap(&childPvValue, childFieldStruct, valueMap, tableName)
					if err != nil {
						return err
					}
				}
				pvValue.Set(childPv.Elem())
			}
		} else {
			err := setColumnValueByMap(&pvValue, fieldStruct, valueMap, "")
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func setColumnValueByMap(childPvValue *reflect.Value, fieldStruct reflect.StructField, valueMap map[string]interface{}, tableName string) error {
	const columnTagName = "column"
	childTagValue := fieldStruct.Tag.Get(columnTagName)
	valueKey := wrapColumnAlias(tableName, childTagValue)
	value, ok := valueMap[valueKey]
	if ok {
		data := value
		dataType := reflect.TypeOf(data)
		if dataType.Kind() == reflect.String && data == "null" {
			return nil
		}
		if childPvValue.Type().Kind() == reflect.Slice && dataType.Kind() == reflect.String {
			sliceData := reflect.New(childPvValue.Type()).Interface()
			dataStr := data.(string)
			if dataStr == "" {
				dataStr = "[]"
			}
			err := json.Unmarshal([]byte(dataStr), &sliceData)
			if err != nil {
				return err
			}
			childPvValue.Set(reflect.ValueOf(sliceData).Elem())
		} else if childPvValue.Type().Kind() == reflect.Map && dataType.Kind() == reflect.String {
			sliceData := reflect.New(childPvValue.Type()).Interface()
			dataStr := data.(string)
			if dataStr == "" {
				dataStr = "{}"
			}
			err := json.Unmarshal([]byte(dataStr), &sliceData)
			if err != nil {
				return err
			}
			childPvValue.Set(reflect.ValueOf(sliceData).Elem())
		} else if dataType == childPvValue.Type() {
			childPvValue.Set(reflect.ValueOf(data))
		} else {
			if dataType.ConvertibleTo(childPvValue.Type()) {
				childPvValue.Set(reflect.ValueOf(data).Convert(childPvValue.Type()))
			} else {
				return errors.New(fmt.Sprintf("table %s  type %s mismatch dataType: %s", tableName, valueKey, dataType.String()))
			}
		}
	}
	return nil
}

func wrapMapSelectCols(entity IEntityStruct) string {
	const zormTagName = "zorm"
	const tableTagName = "table"
	colStr := strings.Builder{}
	entityValue := reflect.ValueOf(entity)
	kind := entityValue.Kind()
	if kind == reflect.Ptr {
		entityType := entityValue.Elem().Type()
		for i := 0; i < entityType.NumField(); i++ {
			fieldStruct := entityType.Field(i)
			zormTagValue := fieldStruct.Tag.Get(zormTagName)
			if zormTagValue == "extends" {
				tableName := fieldStruct.Tag.Get(tableTagName)
				if fieldStruct.Type.Kind() == reflect.Struct {
					childType := fieldStruct.Type
					for j := 0; j < childType.NumField(); j++ {
						childFieldStruct := childType.Field(j)
						wrapSelectCol(&colStr, childFieldStruct, tableName)
					}
				}
			} else {
				wrapSelectCol(&colStr, fieldStruct, "")
			}
		}
	}
	return colStr.String()
}

func wrapSelectCol(colStr *strings.Builder, field reflect.StructField, tableName string) {
	const tagName = "column"
	tagValue := field.Tag.Get(tagName)
	if tagValue != "" {
		if colStr.Len() > 0 {
			colStr.WriteString(" , ")
		}
		if tableName != "" {
			colStr.WriteString(fmt.Sprintf(" %s.%s as %s ", tableName, tagValue, wrapColumnAlias(tableName, tagValue)))
		} else {
			colStr.WriteString(fmt.Sprintf(" %s ", wrapColumnAlias(tableName, tagValue)))
		}
	}
}

func wrapColumnAlias(tableName string, columnName string) string {
	var columnAlias string
	if tableName == "" {
		columnAlias = columnName
	} else {
		columnAlias = fmt.Sprintf("table_%s_%s", tableName, columnName)
	}

	return strings.ToLower(columnAlias)
}

func genInsertSql(entity IEntityStruct) (*Finder, error) {

	sqlStr := strings.Builder{}
	sqlStr.WriteString("INSERT INTO " + entity.GetTableName())
	sqlStr.WriteString(" ( ")

	const tagName = "column"
	colStr := strings.Builder{}
	entityValue := reflect.ValueOf(entity)
	kind := entityValue.Kind()
	var args []interface{}
	if kind == reflect.Ptr {
		entityType := entityValue.Elem().Type()
		for i := 0; i < entityType.NumField(); i++ {
			fieldStruct := entityType.Field(i)
			tagValue := fieldStruct.Tag.Get(tagName)
			if tagValue != "" && tagValue != entity.GetPKColumnName() {
				field := entityValue.Elem().Field(i)
				isDefaultTime,colValue, err := handleSliceOrMapCol(field)
				if isDefaultTime{
					continue
				}
				if err != nil {
					return nil, err
				}
				args = append(args, colValue)
				if colStr.Len() > 0 {
					colStr.WriteString(" , " + tagValue)
				} else {
					colStr.WriteString(tagValue)
				}
			}

		}
	}
	sqlStr.WriteString(colStr.String())
	sqlStr.WriteString(" ) ")
	sqlStr.WriteString(" VALUES ( ")
	valueColsStr := strings.Builder{}
	for i := 0; i < len(args); i++ {
		if valueColsStr.Len() > 0 {
			valueColsStr.WriteString(" , ?")
		} else {
			valueColsStr.WriteString(" ? ")
		}
	}
	sqlStr.WriteString(valueColsStr.String())
	sqlStr.WriteString(" ) ")
	insertFinder := NewFinder()
	insertFinder.Append(sqlStr.String(), args...)
	return insertFinder, nil
}

func handleSliceOrMapCol(field reflect.Value) (bool,interface{}, error) {
	var val interface{}
	if field.Type().Kind() == reflect.Slice || field.Type().Kind() == reflect.Map {
		fieldValue, err := json.Marshal(field.Interface())
		fieldValueStr := string(fieldValue)
		if fieldValueStr == "null" {
			fieldValueStr = ""
		}
		if err != nil {
			return false,nil, errors.New(fmt.Sprintf("%s cannot match type %s", field.Interface(), field.Type().Kind().String()))
		}
		val= fieldValueStr
	} else {
		val= field.Interface()
	}
	colTime,ok:=val.(time.Time)
	if ok{
		if colTime.IsZero(){
			return true,nil,nil
		}
	}
	return false,val,nil
}

func GetSliceItem(list []string, value string) int {
	for i, item := range list {
		if item == value {
			return i
		}
	}
	return -1
}
