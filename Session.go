package zorm

import (
	"context"
	"fmt"
	"log"
	"time"
)

type Session struct {
	finderTable   *Finder
	finderWhere   *Finder
	finderJoin    *Finder
	finderOrderBy *Finder
	finderSet     *Finder
	page          *Page
	engine        *Engine
}

func (session *Session) Where(where string, args ...interface{}) *Session {
	session.checkWhere("and")

	session.appendWhere(where, args...)
	return session
}

func (session *Session) Id(id interface{}) *Session {
	session.checkWhere("and")
	session.appendWhere("id=?", id)
	return session
}

func (session *Session) And(where string, args ...interface{}) *Session {
	session.checkWhere("and")
	session.appendWhere(where, args...)
	return session
}

func (session *Session) Or(where string, args ...interface{}) *Session {
	session.checkWhere("or")
	session.appendWhere(where, args...)
	return session
}

func (session *Session) In(column string, args ...interface{}) *Session {
	session.checkWhere("and")
	if calcLen(args) > 0 {
		whereSql := fmt.Sprintf(" %s in (?) ", column)
		session.appendWhere(whereSql, args)
	} else {
		session.appendWhere(" false ")
	}

	return session
}

func (session *Session) Join(joinOperator string, tableName string, condition string, args ...interface{}) *Session {
	session.checkJoin()
	session.finderJoin.Append(fmt.Sprintf(" %s join %s on %s", joinOperator, tableName, condition), args...)
	return session
}

func (session *Session) SetEntity(entity IEntityStruct) *Session {
	autoSetTime(entity, TagMap{"updated", time.Now()})
	session.checkSet()
	colStr, args := autoSetEntity(entity)
	session.finderSet.Append(colStr, args...)
	return session
}

func (session *Session) SetColsByEntity(entity IEntityStruct, cols ...string) *Session {
	columnValue := autoSetTime(entity, TagMap{"updated", time.Now()})
	if GetSliceItem(cols, columnValue) == -1 {
		cols = append(cols, columnValue)
	}

	session.checkSet()
	colStr, args := autoSetCols(entity, cols...)
	session.finderSet.Append(colStr, args...)
	return session
}

func (session *Session) SetColsByMap(entityMap map[string]interface{}) *Session {
	session.checkSet()
	colStr, args := autoSetColsByMap(entityMap)
	session.finderSet.Append(colStr, args...)
	return session
}

func (session *Session) SetColsStr(colsStr string, args ...interface{}) *Session {
	session.checkSet()
	session.finderSet.Append(colsStr, args...)
	return session
}

func (session *Session) Asc(col string) *Session {
	session.checkOrderBy()
	session.finderOrderBy.Append(col + " asc")
	return session
}

func (session *Session) OrderBy(col string) *Session {
	session.checkOrderBy()
	session.finderOrderBy.Append(col + " asc")
	return session
}

func (session *Session) Desc(col string) *Session {
	session.checkOrderBy()
	session.finderOrderBy.Append(col + " desc")
	return session
}

func (session *Session) Limit(pageSize int, offset int) *Session {
	session.page = NewPage()
	if pageSize != 0 {
		pageNo := offset/pageSize + 1
		if pageNo < 0 {
			pageNo = 0
		}
		session.page.PageNo = pageNo
		session.page.PageSize = pageSize
		return session
	}
	log.Fatalf("pageSize is zero")
	return nil

}

func (session *Session) Count() (int64, error) {
	var err error
	var total int64
	finder := NewFinder()
	finder.Append("select count(1) from (")
	finder, err = finder.AppendFinder(session.genFinder())
	if err != nil {
		return -1, err
	}
	finder.Append(")")


	_, err = QueryRow(session.engine.ctx, finder, &total)
	if err != nil {
		return -1, err
	}
	return total, nil
}

func (session *Session) FindOne( entity interface{}) (bool, error) {

	return QueryRow(session.engine.ctx, session.genFinder(), entity)
}

func (session *Session) FindAll( entity interface{}) error {
	var err error



	err = Query(session.engine.ctx, session.genFinder(), entity, session.page)
	SetNilList(entity)
	return err
}
func (session *Session) FindMap( entity interface{}) error {
	var err error


	listMap, err := QueryMap(session.engine.ctx, session.genFinder(), session.page)
	if err != nil {
		return err
	}
	err = setSliceValueByMap(entity, listMap)
	if err != nil {
		return err
	}
	SetNilList(entity)
	return err
}
func (session *Session) FindOneMap(entity interface{}) (bool, error) {
	var err error


	rowMap, err := QueryRowMap(session.engine.ctx, session.genFinder())
	if rowMap == nil || len(rowMap) == 0 {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	return true, setObjValueByMap(entity, rowMap)

}
func (session *Session) FindString(sql string, args ...interface{}) ([]map[string]interface{}, error) {
	var err error

	finder := NewFinder()
	finder.Append(sql, args)

	listMap, err := QueryMap(session.engine.ctx, finder, nil)
	SetNilList(listMap)
	return listMap, err
}

func (session *Session) Commit() error {
	var err error

	_, err = Transaction(session.engine.ctx, func(ctx context.Context) (interface{}, error) {
		_, err := UpdateFinder(session.engine.ctx, session.genFinder())
		return nil, err
	})
	return err
}